<?php 
include_once 'functions/connection.php';
session_start();

if(isset($_SESSION['user_id'])){
?>
<!DOCTYPE html>
<html>
<?php include_once 'include_once/head.php'; ?>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">

    <?php include_once 'include_once/nav.php'; ?>
    <?php include_once 'include_once/side-nav.php'; ?>

    <div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Home</h1>
            </div>
          </div>
        </div>
    </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <section class="col-lg-8">
            <div class="card">
              <div class="card-body">
                <div id='map'></div>
              </div>
            </div>
          </section>
          <section class="col-lg-4">
          <div class="card">
              <div class="card-header">
                <h3 class="card-title">List of Devices</h3>
              </div>
              <div class="card-body table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                    <th>Name</th>
                    <th>Location</th>
                    <th>Status</th>
                    <th></th>
                  </tr>
                  </thead>
                  <tbody id="results">
                </tbody>
              </table>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  </div>
  <?php include_once 'include_once/footer.php'; ?>
  </div>

  <?php include_once 'include_once/scripts.php'; ?>

<script>
  mapboxgl.accessToken = 'pk.eyJ1Ijoicmt2bG9yZW56bzI2IiwiYSI6ImNpcjk4d3ZmNDAxMXRnOG5rNjVwNzg0ZjMifQ.T2PZrGi-KrZ5u33qj3tlqQ';
  var map = new mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v9',
      center: [120.96079447, 14.3248024], // starting position [lng, lat]
      zoom: 15
  });

  // Add zoom and rotation controls to the map.
  map.addControl(new mapboxgl.NavigationControl());

  var url = 'functions/get_current_loc.php';
    map.on('load', function () {
        window.setInterval(function() {
        map.getSource('drone').setData(url);
    }, 2000);

    map.addSource('drone', { type: 'geojson', data: url });
    map.addLayer({
      "id": "drone",
      "type": "symbol",
      "source": "drone",
      "layout": {
            "icon-image": "{icon}-15",
            "text-field": "{title}",
            "text-font": ["Open Sans Semibold", "Arial Unicode MS Bold"],
            "text-offset": [0, 0.6],
            "text-anchor": "top"
        }
      });

      $.ajax({
          url: 'functions/get_geofence.php',
          type: 'GET',
          success: function(response) {
            var myResponse = JSON.parse(response);
            if (myResponse.message == "success") {
              var data = myResponse.data;
              for(var x = 0; x < data.length; x++) {
                var geoJSON = data[x].poly[0]; 
                var color = data[x].color;

                map.addLayer({
                  'id': 'geofence-'+x,
                  'type': 'line',
                  'source': {
                    'type': 'geojson',
                    'data': geoJSON
                  },
                  'layout': {},
                  'paint': {
                    'line-color': color,
                    'line-width': 5,
                    'line-opacity': .8
                  }
                });
              }
            }
          }
      });
    });

    setInterval(function(){
      getOnline();
      // getPendingGeofence();
    },5000);

    function getPendingGeofence(){
      $.ajax({
          url: 'functions/get_pending_sticker_validation.php',
          type: 'GET',
          success: function(response) {
              var json = JSON.parse(response);
              if (json.message == "success") {
                  var data = json.data;
                  for (var x = 0; x < data.length; x++) {
                      checkGeofence(data[x]);
                  }
              }
          }
      });
    }

    function checkGeofence(locationData) {
      $.ajax({
          url: 'functions/get_geojson.php?id=1',
          type: 'GET',
          success: function(response) {
              var myResponse = JSON.parse(response);
              var data = myResponse.poly;
              var ptsWithin = false;
              for(var x = 0; x < data.length; x++) {
                  var geoJSON = data[x];
                  var color = data[x].color;
                  var location = locationData.Location.split(",");
                  var coor = [location[1],location[0]];
                  var pt = turf.point(coor);
                  if(!ptsWithin) {
                      ptsWithin = turf.inside(pt, geoJSON);
                      if (ptsWithin) {
                        updateSticketValidationStatus(locationData, ptsWithin);
                      }
                  }
              }
          }
      });
    }

    function updateSticketValidationStatus(locationData, ptsWithin){
      var data = {
          "id": locationData.Id,
          "ptsWithin": ptsWithin
      }

      $.ajax({
          url: 'functions/update_sticker_validation.php',
          type: 'POST',
          data: data,
          success: function(response) {
              console.log(response);
          }
      });
    }

    function getOnline(){
      $.ajax({
        url: "functions/get_online.php",
        cache: false,
        success: function(html){
          $("#results").html(html);
        }
      });
    }

    function relocate(val){
      var loc = val.split(",");

      map.flyTo({
          center: [loc[0],loc[1]],
          zoom: 16
              
      });
    }  
</script>
</body>
</html>
<?php
}else{
  header("location:login.php");
}
?>
