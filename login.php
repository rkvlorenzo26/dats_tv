<?php 
include_once 'functions/connection.php';

session_start();
if(isset($_POST['submit'])){
    $user = $_POST['username'];
    $pass = $_POST['password'];
 
    $check = mysqli_query($con,"Select * from tbl_accounts where Username = '$user' AND Password = '$pass'");
    $count = mysqli_num_rows($check);

    if($count == 0){
      ?>
        <script>alert("Incorrect username or password!");</script>
      <?php
    }else{
      $row = mysqli_fetch_array($check);
      $_SESSION['user_id'] = $row['UserID'];
      $_SESSION['username'] = $row['Username'];

      header("location: index.php");
    }
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DATS-TV | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="icon" href="dist/logo.png">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="login.php"><b>DATS-TV</b></a>
  </div>
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <form method="post">
        <div class="form-group has-feedback">
          <input name="username" type="text" class="form-control" placeholder="Username" required>
        </div>
        <div class="form-group has-feedback">
          <input name="password" type="password" class="form-control" placeholder="Password" required>
        </div>
        <div class="row">
            <button name="submit" type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
      </form>
    </div>
  </div>
</div>

<script src="plugins/jquery/jquery.min.js"></script>
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
