<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <a href="index.php" class="brand-link">
    <img src="dist/logo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
          style="opacity: .8">
    <span class="brand-text font-weight-light">DATS-TV</span>
  </a>

  <div class="sidebar">
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-item">
          <a id="home" href="index.php" class="nav-link">
            <i class="nav-icon fa fa-home"></i>
            <p>
              Home
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a id="home" href="speed-tracker.php" class="nav-link">
            <i class="nav-icon fa fa-clock-o"></i>
            <p>
              Speed Tracker
            </p>
          </a>
        </li>

        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fa fa-wpforms"></i>
            <p>
            Violations
              <i class="fa fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview" style="display: none;">
            <li class="nav-item">
              <a id="main-geofence" href="violation-sticker.php" class="nav-link">
                <p>Expired Sticker</p>
              </a>
            </li>
            <li class="nav-item">
              <a id="main-geofence" href="violation-parking.php" class="nav-link">
                <p>Illegal Parking</p>
              </a>
            </li>
            <li class="nav-item">
              <a id="main-geofence" href="violation-speed.php" class="nav-link">
                <p>Overspeeding</p>
              </a>
            </li>
          </ul>          

        </li>

        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fa fa-edit"></i>
            <p>
            Geofence
              <i class="fa fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview" style="display: none;">
            <li class="nav-item">
              <a id="main-geofence" href="main-geofence.php" class="nav-link">
                <p>Main Geofence</p>
              </a>
            </li>
            <li class="nav-item">
              <a id="faculty-geofence" href="faculty-geofence.php" class="nav-link">
              <p>Faculty Parking Area</p>
              </a>
            </li>
            <li class="nav-item">
              <a id="student-geofence" href="student-geofence.php" class="nav-link">
              <p>Student Parking Area</p>
              </a>
            </li>
          </ul>
        </li>

        <li class="nav-item">
          <a id="device-management" href="device-management.php" class="nav-link">
            <i class="nav-icon fa fa-list"></i>
            <p>
              Device Management
            </p>
          </a>
        </li>

        <li class="nav-item">
          <a href="functions/logout.php" class="nav-link">
            <i class="nav-icon fa fa-sign-out"></i>
            <p>
              Logout
            </p>
          </a>
        </li>
      </ul>
    </nav>
  </div>
</aside>