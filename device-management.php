<?php 
include_once 'functions/connection.php';
session_start();

if(isset($_SESSION['user_id'])){
?>
<!DOCTYPE html>
<html>
<?php include_once 'include_once/head.php'; ?>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <?php include_once 'include_once/nav.php'; ?>
    <?php include_once 'include_once/side-nav.php'; ?>

    <div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Device Management</h1>
            </div>
          </div>
        </div>
    </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <section class="col-lg-12">
            <div class="card">
              <div class="card-body table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                    <th>TrackerID</th>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Email</th>
                    <th>Expiration Date</th>
                    <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      $get_devices = mysqli_query($con, 
                            "SELECT 
                              tbl_device.TrackerID,
                              tbl_device.Name,
                              tbl_device.TrackerType,
                              tbl_device.Email,
                              tbl_device.Expiration_Date
                             FROM tbl_device order by TrackerID");
                      while ($row = mysqli_fetch_array($get_devices)) { 
                        echo "<tr>";
                        echo "<td>". $row['TrackerID'] ."</td>";
                        echo "<td>". $row['Name'] ."</td>";
                        echo "<td>". $row['TrackerType'] ."</td>";
                        echo "<td>". $row['Email'] ."</td>";
                        echo "<td>". $row['Expiration_Date'] ."</td>";
                        echo "<td><center><a href='javascript:void(0)' onclick='edit(".$row['TrackerID'].")' class='btn btn-primary'>Edit</a></center></td>";
                        echo "</tr>";
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>

    <div class="modal fade" id="editModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              Edit Device
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
              <form method="POST" action="functions/save_tracker.php">
                  <input name="id" id="id" type="text" hidden>
                  <div class="form-group">
                    <label>Name:</label>
                    <input name="name" id="name" type="text" placeholder="Enter Name" class="form-control" autocomplete="off" required>
                  </div>
                  <div class="form-group">
                    <label>Type:</label>
                    <select name="type" id="type" class="form-control">
                      <option value="Faculty">Faculty</option>
                      <option value="Student">Student</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label>Email Address:</label>
                    <input name="email" id="email" type="text" placeholder="Email Address" class="form-control" autocomplete="off" required>
                  </div>
                  <div class="form-group">
                    <label>Expiration Date:</label>
                    <input name="expiration_date" id="expiration_date" type="date" class="form-control" required>
                  </div>
                  <center><input name="color_change" type="submit" class="btn btn-primary"></center>
                </form> 
          </div>
        </div>
      </div>
    </div>
  </div>
  <?php include_once 'include_once/footer.php'; ?>
  </div>
  <?php include_once 'include_once/scripts.php'; ?>
<script>
  function edit(TrackerID) {
    $.ajax({
      url: 'functions/get_tracker_info.php',
      type: "POST",
      data: {TrackerID: TrackerID},
      success: function(response){
        res = JSON.parse(response);
        if (res.message == "success") {
          $("#id").val(res.data[0].id);
          $("#name").val(res.data[0].name);
          $("#type").val(res.data[0].type);
          $("#email").val(res.data[0].email);
          $("#expiration_date").val(res.data[0].expiration);

          $('#editModal').modal('show'); 
        } 
      }
    });
  }
</script>
</body>
</html>
<?php
}else{
  header("location:login.php");
}
?>
