<?php 
include_once 'functions/connection.php';
session_start();
if(isset($_SESSION['user_id'])){
  if(isset($_POST['color_change'])){
    $color = $_POST['color'];

    $update = mysqli_query($con,"UPDATE tbl_geofence set Color = '$color' where id ='2'");
    if($update){
      header("location: geofence.php?change_color=success");
    }
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DATS-TV</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="icon" href="dist/logo.png">

  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.45.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.45.0/mapbox-gl.css' rel='stylesheet' />
  <script src='https://api.mapbox.com/mapbox.js/plugins/turf/v3.0.11/turf.min.js'></script>
  <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.js'></script>
  <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.css' type='text/css'/>
  <style>
     #map { top:0; bottom:0; width:100%; height: 650px; }
  </style>
  <style>
    .calculation-box {
        height: 75px;
        width: 150px;
        position: absolute;
        bottom: 40px;
        left: 10px;
        background-color: rgba(255, 255, 255, .9);
        padding: 15px;
        text-align: center;
    }
</style>
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <?php include_once 'include_once/nav.php'; ?>
  <?php include_once 'include_once/side-nav.php'; ?>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Faculty Parking Area</h1>
          </div>
      </div>
    </div>
  </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <section class="col-lg-12">
          <div class="card">
              <div class="card-header">
                <div class="pull-right">
                  <a href="geofence_edit.php?id=2" class="btn btn-success">Edit Geofence</a>
                </div>
              </div>
              <div class="card-body">
                <div id='map'></div>
              </div>
            </div>
          </section>
        </div>

        <div class="modal fade" id="colorModal">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                  Change Color
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                  <form method="POST">
                    <?php
                      $color = mysqli_query($con,"SELECT * from tbl_geofence");
                      $row = mysqli_fetch_array($color);
                    ?>
                      <label>Color:</label><input name="color" value="<?php echo $row['Color']; ?>" type="text" placeholder="Enter Color" class="form-control" autocomplete="off" required><br>
                      <center><input name="color_change" type="submit" class="btn btn-primary"></center>
                    </form> 
              </div>
            </div>
          </div>
        </div>

      </div>
    </section>

  </div>
  <?php include_once 'include_once/footer.php' ?>
</div>

<?php include_once 'include_once/scripts.php' ?>

<script>
mapboxgl.accessToken = 'pk.eyJ1Ijoicmt2bG9yZW56bzI2IiwiYSI6ImNpcjk4d3ZmNDAxMXRnOG5rNjVwNzg0ZjMifQ.T2PZrGi-KrZ5u33qj3tlqQ';
/* eslint-disable */
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v9',
    center: [120.96079447, 14.3248024], // starting position [lng, lat]
    zoom: 15
});

// Add zoom and rotation controls to the map.
map.addControl(new mapboxgl.NavigationControl());

map.on('load', function () {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {

          var myResponse = JSON.parse(this.responseText);

          var geoJSON = myResponse.poly[0];
          var color = myResponse.color;

          map.addLayer({
              'id': 'maine',
              'type': 'line',
              'source': {
                'type': 'geojson',
                'data': geoJSON
              },
              'layout': {},
              'paint': {
                'line-color': color,
                'line-width': 5,
                'line-opacity': .8
              }
          });
      }
  };
  xmlhttp.open("GET", "functions/get_geojson.php?id=2", true);
  xmlhttp.send();
});

</script>


</body>
</html>
<?php
}else{
  header("location:login.php");
}
?>
