<?php 
include_once 'functions/connection.php';
session_start();
if(isset($_SESSION['user_id'])){
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DATS-TV</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="plugins/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link rel="icon" href="dist/logo.png">

  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.45.0/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.45.0/mapbox-gl.css' rel='stylesheet' />
  <script src='https://api.mapbox.com/mapbox.js/plugins/turf/v3.0.11/turf.min.js'></script>
  <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.js'></script>
	<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-draw/v1.0.9/mapbox-gl-draw.css' type='text/css'/>
  <style>
     #map { top:0; bottom:0; width:100%; height: 650px; }
  </style>
  <style>
    .calculation-box {
        height: 75px;
        width: 350px;
        position: absolute;
        bottom: 40px;
        left: 10px;
        margin-left: 20px;
        background-color: rgba(255, 255, 255, .9);
        padding: 15px;
        text-align: center;
    }
</style>


</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <?php include_once 'include_once/nav.php'; ?>
  <?php include_once 'include_once/side-nav.php'; ?>

  <div class="content-wrapper">
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit Geofence</h1>
          </div>
        </div>
      </div>
    </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <section class="col-lg-12">
            <div class="card">
              <div class="card-body">
                <div id='map'></div>
                <div class='calculation-box'>
                    <div id='calculated-area'>Draw a polygon using the draw tools. And save it by clicking the draw tool again</div>
                </div>
              </div>
            </div>
          </section>

        </div>
      </div>
    </section>
  </div>
  
  <?php include_once 'include_once/footer.php' ?>
  </div>

<?php include_once 'include_once/scripts.php' ?>

<script>
mapboxgl.accessToken = 'pk.eyJ1Ijoicmt2bG9yZW56bzI2IiwiYSI6ImNpcjk4d3ZmNDAxMXRnOG5rNjVwNzg0ZjMifQ.T2PZrGi-KrZ5u33qj3tlqQ';
/* eslint-disable */
var map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v9',
    center: [120.96079447, 14.3248024], // starting position [lng, lat]
    zoom: 15
});

var draw = new MapboxDraw({
    displayControlsDefault: false,
    controls: {
        polygon: true,
        trash: true
    }
});
map.addControl(draw);

map.on('draw.create', updateArea);
map.on('draw.delete', updateArea);
map.on('draw.update', updateArea);

function updateArea(e) {
    var data = draw.getAll();
    var answer = document.getElementById('calculated-area');
    if (data.features.length > 0) {
        var area = turf.area(data);
        // restrict to area to 2 decimal points
        var rounded_area = Math.round(area*100)/100;
        answer.innerHTML = '<p><strong>' + rounded_area + '</strong></p><p>square meters</p>';

        var x = data.features[0];
        var geojson = JSON.stringify(x['geometry']);

        var url = new URL(window.location);
        var id = url.searchParams.get("id");
        window.location="functions/geofence_update.php?data="+geojson+"&id="+id;
    } else {
        answer.innerHTML = '';
        if (e.type !== 'draw.delete') alert("Use the draw tools to draw a polygon!");
    }
}
</script>


</body>
</html>
<?php
}else{
  header("location:login.php");
}
?>
