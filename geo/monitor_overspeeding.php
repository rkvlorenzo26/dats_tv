<?php

include_once '../functions/connection.php';

require_once __DIR__ . "/vendor/autoload.php";

use Location\Coordinate;
use Location\Polygon;
use Location\Distance\Vincenty;

$geofence = new Polygon();

$date = date('Y-m-d'); //Current Date

$get_devices = mysqli_query($con, "SELECT DISTINCT TrackerID FROM tbl_location WHERE DATE(Date) = '$date'");
$count_devices = mysqli_num_rows($get_devices);
if ($count_devices > 0) {
    //Get Main Geofence
    $get_main_geo = mysqli_query($con, "SELECT * FROM tbl_geofence WHERE ID = 1");
    $row = mysqli_fetch_array($get_main_geo);
    $json = json_decode($row['Geofence'], true);
    $coordinates = $json['coordinates'][0];

    //Create Geofence
    foreach($coordinates as $coordinate) {
        $geofence->addPoint(new Coordinate(number_format($coordinate[1], 15),number_format($coordinate[0], 14)));
    }

    while ($device = mysqli_fetch_array($get_devices)) {
        $toDate = date('Y-m-d H:i:s');
        $time = strtotime($toDate);
        $time = $time - (5 * 60);
        $fromDate = date("Y-m-d H:i:s", $time);

        $device_id = $device['TrackerID'];
        $get_locations = mysqli_query($con, "SELECT * FROM tbl_location WHERE TrackerID = '$device_id' AND Date >= '$fromDate' ORDER BY Date ASC");
        $count_rows = mysqli_num_rows($get_locations);

        if ($count_rows > 0) {
            $counter = 0;
            $latitude;
            $longitude;
            $fromTime;
            $isOverspeed = false;

            $get_limit = mysqli_query($con, "SELECT Value FROM tbl_defaults WHERE Name = 'speed_limit'");
            $limit = mysqli_fetch_array($get_limit);
            $speed_limit = $limit['Value'];

            while ($row = mysqli_fetch_array($get_locations)) {
                if ($counter == 0) {
                    $location = explode(",", $row['Location']);
                    $latitude = $location[0];
                    $longitude = $location[1];
                    $fromTime = $row['Date'];
                    $counter++;
                } else {
                    $coordinate1 = new Coordinate($latitude, $longitude);
                    $toTime = $row['Date'];
                    $location = explode(",", $row['Location']);
                    $lat = $location[0];
                    $long = $location[1];
                    $coordinate2 = new Coordinate($lat, $long);

                    $meter_per_second = ($speed_limit * 1000) / 3600; //Get Meter per Second; Based on Speed Limit set;
                    $timeTraveledInSec = abs(strtotime($toTime) - strtotime($fromTime));    //Get difference of time in sec
                    $expectedDistance = $meter_per_second * $timeTraveledInSec;
                    $computedDistance = var_export($coordinate1->getDistance($coordinate2, new Vincenty()), true);
                    $isOverspeed = $expectedDistance < $computedDistance;

                    $speedInMeterPerSeconds = $computedDistance / $timeTraveledInSec;
                    $speedInKmsPerHr = $speedInMeterPerSeconds * 3.6; //1 meter/sec = 3.6 km/hr
                    $remarks = $speedInKmsPerHr . " km/hr";

                    $latitude = $lat;
                    $longitude = $long;
                    $fromTime = $toTime;

                    if ($isOverspeed) {
                        $get_details = mysqli_query($con, "SELECT Email FROM tbl_device WHERE TrackerID = '$device_id'");
                        $validate_details = mysqli_num_rows($get_details);
                        if ($validate_details > 0) {
                            $details = mysqli_fetch_array($get_details);
                            $email = $details['Email'];
        
                                $msg = '
                                <p>Good Day,</p>
                                <p>You exceeded the speed limit. Please report to GSO.</p>
                                ';
                            
                                // use wordwrap() if lines are longer than 70 characters
                                $msg = wordwrap($msg,70);
                            
                                // To send HTML mail, the Content-type header must be set
                                $headers  = 'MIME-Version: 1.0' . "\r\n";
                                $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                                  
                                // send email
                                mail($email, "Overspeeding", $msg, $headers);
                            
                                $now = date('Y-m-d H:i:s');
                                $insert = mysqli_query($con, "INSERT INTO tbl_email(TrackerID, Type, Date, Remarks) values ('$device_id', 'Overspeeding', '$now', '$remarks')");
                        }
                    }
                }
            }
        }
    }
}
?>