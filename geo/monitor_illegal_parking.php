<?php

include_once '../functions/connection.php';

require_once __DIR__ . "/vendor/autoload.php";

use Location\Coordinate;
use Location\Polygon;
use Location\Distance\Vincenty;

$geofence = new Polygon();

$date = date('Y-m-d'); //Current Date

$get_devices = mysqli_query($con, "SELECT DISTINCT tbl_location.TrackerID FROM tbl_location 
                                    LEFT JOIN tbl_device ON tbl_location.TrackerID = tbl_device.TrackerID
                                    WHERE DATE(Date) = '$date' AND tbl_device.TrackerType = 'Student'");
$count_devices = mysqli_num_rows($get_devices);
if ($count_devices > 0) {
    //Get Main Geofence
    $get_main_geo = mysqli_query($con, "SELECT * FROM tbl_geofence WHERE ID = 2");
    $row = mysqli_fetch_array($get_main_geo);
    $json = json_decode($row['Geofence'], true);
    $coordinates = $json['coordinates'][0];

    //Create Geofence
    foreach($coordinates as $coordinate) {
        $geofence->addPoint(new Coordinate(number_format($coordinate[1], 15),number_format($coordinate[0], 14)));
    }

    while ($device = mysqli_fetch_array($get_devices)) {
        $toDate = date('Y-m-d H:i:s');
        $time = strtotime($toDate);
        $time = $time - (6 * 60);
        $fromDate = date("Y-m-d H:i:s", $time);

        $device_id = $device['TrackerID'];
        $get_locations = mysqli_query($con, "SELECT * FROM tbl_location WHERE TrackerID = '$device_id' AND Date >= '$fromDate' ORDER BY Date ASC");
        $count_rows = mysqli_num_rows($get_locations);

        if ($count_rows > 0) {
            $get_limit = mysqli_query($con, "SELECT Value FROM tbl_defaults WHERE Name = 'illegal_parking'");
            $limit = mysqli_fetch_array($get_limit);
            $parking_limit = $limit['Value'] * 60;
            $parking_counter = 0;
            $lastTime;
            $parkingStatus = false;

            while ($row = mysqli_fetch_array($get_locations)) {
                $location = explode(",", $row['Location']);
                $latitude = $location[0];
                $longitude = $location[1];
                $currTime = $row['Date'];
                $point = new Coordinate($latitude, $longitude);
                $isInFacultyGeofence = var_export($geofence->contains($point), true);
                if ($isInFacultyGeofence == 'true' && $parkingStatus) {
                    $parkingTime = abs(strtotime($currTime) - strtotime($lastTime));    //Get difference of time in sec
                    $parking_counter = $parking_counter + $parkingTime;
                    $lastTime = $currTime;
                    $parkingStatus = true;
                } else if ($isInFacultyGeofence == 'true' && !$parkingStatus){
                    $lastTime = $currTime;
                    $parking_counter = 0;
                    $parkingStatus = true;
                } else {
                    $parkingStatus = false;
                    $parking_counter = 0;
                }
            }

            if ($parking_limit <= $parking_counter) {
                $get_details = mysqli_query($con, "SELECT Email FROM tbl_device WHERE TrackerID = '$device_id'");
                $validate_details = mysqli_num_rows($get_details);
                if ($validate_details > 0) {
                    $details = mysqli_fetch_array($get_details);
                    $email = $details['Email'];

                    $get_email_service = mysqli_query($con, "SELECT * FROM tbl_email WHERE TrackerID = '$device_id' 
                    AND DATE(Date) = '$date' AND Type = 'Illegal Parking'");
                    $count_email = mysqli_num_rows($get_email_service);

                    if ($count_email == 0) {
                        $msg = '
                        <p>Good Day,</p>
                        <p>You parked at the Faculty Parking Area. Please report to GSO to settle your account.</p>
                        ';
                    
                        // use wordwrap() if lines are longer than 70 characters
                        $msg = wordwrap($msg,70);
                    
                        // To send HTML mail, the Content-type header must be set
                        $headers  = 'MIME-Version: 1.0' . "\r\n";
                        $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                          
                        // send email
                        mail($email, "Illegal Parking", $msg, $headers);
                    
                        $now = date('Y-m-d H:i:s');
                        $insert = mysqli_query($con, "INSERT INTO tbl_email(TrackerID, Type, Date) values ('$device_id', 'Illegal Parking', '$now')");
                    }
                }
            }
        }

    }
}
?>