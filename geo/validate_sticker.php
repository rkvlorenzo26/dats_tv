<?php

include_once '../functions/connection.php';

require_once __DIR__ . "/vendor/autoload.php";

use Location\Coordinate;
use Location\Polygon;

$date = date('Y-m-d');
$geofence = new Polygon();

//Get Main Geofence
$get_main_geo = mysqli_query($con, "SELECT * FROM tbl_geofence WHERE ID = 1");
$row = mysqli_fetch_array($get_main_geo);
$json = json_decode($row['Geofence'], true);
$coordinates = $json['coordinates'][0];

//Create Geofence
foreach($coordinates as $coordinate) {
    $geofence->addPoint(new Coordinate(number_format($coordinate[1], 15),number_format($coordinate[0], 14)));
}

//Query in getting all pending valid sticker
$get_pending = mysqli_query($con, "SELECT * FROM tbl_location WHERE isValidSticker IS NULL AND DATE(Date) = '$date'");
$count = mysqli_num_rows($get_pending);
if ($count > 0) {
    while($row = mysqli_fetch_array($get_pending)) {
        $id = $row['ID'];
        $location = explode(",", $row['Location']);
        $latitude = $location[0];
        $longitude = $location[1];

        $point = new Coordinate($latitude, $longitude);
        $isInMainGeofence = var_export($geofence->contains($point), true);
        if ($isInMainGeofence == 'true') {  // If location is within the main geofence; Validate Sticker;
            $get_details = mysqli_query($con, "SELECT Email, Expiration_Date, tbl_device.TrackerID FROM tbl_location 
                    JOIN tbl_device ON tbl_location.TrackerID = 
                    tbl_device.TrackerID WHERE tbl_location.ID = '$id' AND Expiration_Date < '$date'");
            $validate_details = mysqli_num_rows($get_details);
            if ($validate_details > 0) {
                $details = mysqli_fetch_array($get_details);
                $email = $details['Email'];
                $tracker = $details['TrackerID'];

                $get_email_service = mysqli_query($con, "SELECT * FROM tbl_email WHERE TrackerID = '$tracker' 
                                                            AND DATE(Date) = '$date' AND Type = 'Expired Sticker'");
                $count_email = mysqli_num_rows($get_email_service);

                if ($count_email == 0) {
                    $msg = '
                    <p>Hello,</p>
                    <p>Our system detected that your sticker is already expired. Please renew it immediately.</p>
                    ';
                
                    // use wordwrap() if lines are longer than 70 characters
                    $msg = wordwrap($msg,70);
                
                    // To send HTML mail, the Content-type header must be set
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
                      
                    // send email
                    mail($email, "Sticker Already Expired", $msg, $headers);
                
                    $now = date('Y-m-d H:i:s');
                    $insert = mysqli_query($con, "INSERT INTO tbl_email(TrackerID, Type, Date) values ('$tracker', 'Expired Sticker', '$now')");
                }
            }

            //Query in saving current location of device
            $update = mysqli_query($con, "UPDATE tbl_location set isValidSticker = '$isInMainGeofence' WHERE ID = '$id'");
        }
    }
}
?>