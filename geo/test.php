<?php

include_once '../functions/connection.php';

require_once __DIR__ . "/vendor/autoload.php";

use Location\Coordinate;
use Location\Polygon;

$geofence = new Polygon();

$get_main_geo = mysqli_query($con, "SELECT * FROM tbl_geofence WHERE ID = 1");
$row = mysqli_fetch_array($get_main_geo);
$json = json_decode($row['Geofence'], true);

$coordinates = $json['coordinates'][0];

foreach($coordinates as $coordinate) {
    $geofence->addPoint(new Coordinate(number_format($coordinate[1], 15),number_format($coordinate[0], 14)));
}

$outsidePoint = new Coordinate(-12.075452, -76.985079);
$insidePoint = new Coordinate(14.32781137, 120.95696584);
$facpoint = new Coordinate(14.325550857665732, 120.95868088928376);

$test = var_export($geofence->contains($outsidePoint), true);
echo $test;
if($test == 'true'){
    echo "AAA";
}

var_dump($geofence->contains($outsidePoint)); // returns bool(false) the point is outside the polygon
var_dump($geofence->contains($insidePoint)); // returns bool(true) the point is inside the polygon
var_dump($geofence->contains($facpoint)); // returns bool(true) the point is inside the polygon
?>