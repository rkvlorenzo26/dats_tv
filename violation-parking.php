<?php 
include_once 'functions/connection.php';
session_start();

if(isset($_SESSION['user_id'])){
?>
<!DOCTYPE html>
<html>
<?php include_once 'include_once/head.php'; ?>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <?php include_once 'include_once/nav.php'; ?>
    <?php include_once 'include_once/side-nav.php'; ?>

    <div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Violation - Illegal Parking</h1>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3">
                <a href="javascript:void(0)" onclick="deleteRecords()" class="btn btn-primary">Delete Records</a>
            </div>
          </div>
        </div>
    </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <section class="col-lg-12">
            <div class="card">
              <div class="card-body table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                    <th>Date</th>
                    <th>TrackerID</th>
                    <th>Type</th>
                    <th>Name</th>
                    <th>Violation</th>
                  </tr>
                  </thead>
                  <tbody>
                    <?php 
                      $get_violations = mysqli_query($con, 
                            "SELECT 
                              tbl_email.Date,
                              tbl_email.TrackerID,
                              tbl_device.TrackerType,
                              tbl_device.Name,
                              tbl_email.Type
                             FROM tbl_email 
                             JOIN tbl_device on tbl_email.TrackerID = tbl_device.TrackerID
                             WHERE tbl_email.Type = 'Illegal Parking'
                             ORDER BY Date DESC");
                      while ($row = mysqli_fetch_array($get_violations)) { 
                        echo "<tr>";
                        echo "<td>". $row['Date'] ."</td>";
                        echo "<td>". $row['TrackerID'] ."</td>";
                        echo "<td>". $row['TrackerType'] ."</td>";
                        echo "<td>". $row['Name'] ."</td>";
                        echo "<td>". $row['Type'] ."</td>";
                        echo "</tr>";
                      }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
    <div class="modal fade" id="deleteModal">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
              Delete Records
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-body">
                Please click confirm to delete all records.
          </div>
          <div class="modal-footer">
            <a href="functions/delete_violation.php?id=2" class="btn btn-primary">Confirm</a>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php include_once 'include_once/footer.php'; ?>
  </div>
  <script>
      function deleteRecords() {
          $('#deleteModal').modal('show'); 
      }
  </script>
  <?php include_once 'include_once/scripts.php'; ?>
</body>
</html>
<?php
}else{
  header("location:login.php");
}
?>
