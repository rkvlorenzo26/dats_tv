<?php 
include_once 'connection.php';
require_once __DIR__ . "/vendor/autoload.php";

use Location\Coordinate;
use Location\Polygon;
use Location\Distance\Vincenty;

$geofence = new Polygon();

$device_id = $_GET['id'];

$date = date('Y-m-d'); //Current Date

$toDate = date('Y-m-d H:i:s');
$time = strtotime($toDate);
$time = $time - (5 * 60);
$fromDate = date("Y-m-d H:i:s", $time);

$get_locations = mysqli_query($con, "SELECT * FROM tbl_location WHERE TrackerID = '$device_id' AND Date >= '$fromDate' ORDER BY Date ASC");
$count_rows = mysqli_num_rows($get_locations);

if ($count_rows > 0) {
    $latitude;
    $longitude;
    $fromTime;
    $isOverspeed = false;

    $counter = 0;
    $get_limit = mysqli_query($con, "SELECT Value FROM tbl_defaults WHERE Name = 'speed_limit'");
    $limit = mysqli_fetch_array($get_limit);
    $speed_limit = $limit['Value'];

    while ($row = mysqli_fetch_array($get_locations)) {
        if ($counter == 0) {
            $location = explode(",", $row['Location']);
            $latitude = $location[0];
            $longitude = $location[1];
            $fromTime = $row['Date'];
            $counter++;
        } else {
            $coordinate1 = new Coordinate($latitude, $longitude);
            $toTime = $row['Date'];
            $location = explode(",", $row['Location']);
            $lat = $location[0];
            $long = $location[1];
            $coordinate2 = new Coordinate($lat, $long);

            $timeTraveledInSec = abs(strtotime($toTime) - strtotime($fromTime));    //Get difference of time in sec
            $computedDistance = var_export($coordinate1->getDistance($coordinate2, new Vincenty()), true);
            $speedInMeterPerSeconds = $computedDistance / $timeTraveledInSec;
            $speedInKmsPerHr = $speedInMeterPerSeconds * 3.6; //1 meter/sec = 3.6 km/hr

            echo "<tr>";
            echo "<td>" . $fromTime . "</td>";
            echo "<td>" . $latitude . ", " . $longitude ."</td>";
            echo "<td>" . $toTime . "</td>";
            echo "<td>" . $lat . ", " . $long ."</td>";
            echo "<td>" . $computedDistance . "</td>";
            echo "<td>" . $speedInKmsPerHr ." km/hr</td>";
            echo "</tr>";

            $latitude = $lat;
            $longitude = $long;
            $fromTime = $toTime;
        }
    }
}
?>