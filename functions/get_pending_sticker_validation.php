<?php 
include_once 'connection.php';

$date = date('Y-m-d');

//Query in getting all pending valid sticker
$get_pending = mysqli_query($con, "SELECT * FROM tbl_location WHERE isValidSticker IS NULL AND DATE(Date) = '$date'");
$count = mysqli_num_rows($get_pending);

$response = array();
if ($count > 0) {
    $data = array();
    while($row = mysqli_fetch_array($get_pending)) {
		$row_arr = array(
            'Id' => $row['ID'],
            'Location' => $row['Location']
        );
        array_push($data, $row_arr);
    }
    $response['data'] = $data;
	$response['message'] = "success";
} else {
    $response['message'] = 'failed';
}

echo json_encode($response);

?>