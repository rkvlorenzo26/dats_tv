<?php 
include_once 'connection.php';
session_start();

$features = array();

$get_dev = mysqli_query($con,"SELECT * from tbl_device ORDER BY TrackerID ASC");
while($row = mysqli_fetch_array($get_dev)){

    $status = mysqli_query($con,"SELECT * from tbl_location where TrackerID = '$row[TrackerID]' ORDER BY Date DESC limit 1");
    $get_status = mysqli_fetch_array($status);
    $lastdate = date("Y-m-d",strtotime($get_status['Date']));
    $now_date = date("Y-m-d");

    $last_time = date("Y-m-d H:i:s",strtotime($get_status['Date']));
    $now_time = date("Y-m-d H:i:s");

    //compute total time
    $to_time = strtotime($now_time);
    $from_time = strtotime($last_time);
    $totaltime = round(abs($to_time - $from_time)/60);

    if($lastdate == $now_date){
        if($totaltime < 5){

            $loc = explode(",", $get_status['Location']);
            $lat = floatval($loc[0]);
            $lang = floatval($loc[1]);

            $date = date("m-d-Y h:i:s", strtotime($get_status['Date']));
            
            $row_arr = array(
                'type' => 'Feature',
                'geometry' => [
                                'type' =>'Point',
                                'coordinates' => [ $lang , $lat ]
                              ],
                'properties' => [
                                    'title' => $row['Name'],
                                    'description'   =>  "Date: ".$date."<br>"."Location: ".$get_status['Location'],
                                    'icon'  =>  'circle'
                                ]
                );
                
            array_push($features, $row_arr);

        }
    }
}

$response = array();
$response['type'] = "FeatureCollection";
$response['features'] = $features;

echo json_encode($response);
?>