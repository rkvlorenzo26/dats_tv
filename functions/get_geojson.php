<?php 
include_once 'connection.php';

$id = $_GET['id'];
$query = mysqli_query($con,"SELECT * from tbl_geofence WHERE ID = '$id'");

$features = array();
$row = mysqli_fetch_array($query);

$row_arr = array(
	'type' => 'Feature',
	'geometry' => json_decode($row['Geofence'])
);
array_push($features, $row_arr);

$response = array();
$response['poly'] = $features;
$response['color'] = $row['Color'];

echo json_encode($response);
?>