<?php 
include_once 'connection.php';

$id = $_POST['TrackerID'];

$query = mysqli_query($con,"SELECT * from tbl_device WHERE TrackerID = '$id'");
$count = mysqli_num_rows($query);

$response = array();
if ($count > 0) {
	$data = array();
	while($row = mysqli_fetch_array($query)) {
		$temp = array();
		$temp['id'] = $row['TrackerID'];
		$temp['name'] = $row['Name'];
		$temp['type'] = $row['TrackerType'];
		$temp['expiration'] = $row['Expiration_Date'];
		$temp['email'] = $row['Email'];
		array_push($data, $temp);
	}
	
	$response['data'] = $data;
	$response['message'] = "success";
} else {
	$response['message'] = 'failed';
}

echo json_encode($response);
?>