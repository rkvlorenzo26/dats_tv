<?php 
include_once 'connection.php';

//Query in getting all the geofence data
$query = mysqli_query($con,"SELECT * from tbl_geofence");
$count = mysqli_num_rows($query);

$response = array();
if ($count > 0) {
	$data = array();
	while($row = mysqli_fetch_array($query)) {
		$features = array();
		$row_arr = array(
			'type' => 'Feature',
			'geometry' => json_decode($row['Geofence'])
		);
		array_push($features, $row_arr);

		$temp = array();
		$temp['poly'] = $features;
		$temp['color'] = $row['Color'];
		array_push($data, $temp);
	}
	
	$response['data'] = $data;
	$response['message'] = "success";
} else {
	$response['message'] = 'failed';
}

echo json_encode($response);
?>