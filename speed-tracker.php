<?php 
include_once 'functions/connection.php';
session_start();

if(isset($_SESSION['user_id'])){
?>
<!DOCTYPE html>
<html>
<?php include_once 'include_once/head.php'; ?>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">
    <?php include_once 'include_once/nav.php'; ?>
    <?php include_once 'include_once/side-nav.php'; ?>

    <div class="content-wrapper">
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1 class="m-0 text-dark">Speed Tracker</h1>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-3">
            Device:
            <select name="devices" id="devices" class="form-control">
                <?php
                  $get_dev = mysqli_query($con,"SELECT * from tbl_device ORDER BY TrackerID ASC");
                  while ($row = mysqli_fetch_array($get_dev)) { ?>
                  <option value="<?php echo $row['TrackerID']; ?>"><?php echo $row['Name']; ?></option>
                <?php  } ?>
              </select>
            </div>
          </div>
        </div>
    </div>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <section class="col-lg-12">
            <div class="card">
              <div class="card-body table-responsive">
                <table class="table table-bordered">
                  <thead>
                    <tr>
                    <th>Time 1</th>
                    <th>Location 1</th>
                    <th>Time 2</th>
                    <th>Location 2</th>
                    <th>Distance</th>
                    <th>Speed</th>
                  </tr>
                  </thead>
                  <tbody id="speedTable">
                  </tbody>
                </table>
              </div>
            </div>
          </section>
        </div>
      </div>
    </section>
  </div>

  <?php include_once 'include_once/footer.php'; ?>
  </div>
  <?php include_once 'include_once/scripts.php'; ?>
</body>
<script>
setInterval(function(){
  getLatestSpeedOfDevice();
},1000);

function getLatestSpeedOfDevice(){
  var id = $('#devices option:selected').val();
  $.ajax({
    url: "functions/get_device_speed.php?id=" + id,
    cache: false,
    success: function(html){
      $("#speedTable").html(html);
    }
  });
}
</script>
</html>
<?php
}else{
  header("location:login.php");
}
?>
